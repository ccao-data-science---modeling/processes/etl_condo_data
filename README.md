# Goal

This repository contains the scripts and files necessary to create the two major tabular datasets, `modeldata` and `assmntdata`, that are used to [calculate assessed values for all Cook County residential condominiums](https://gitlab.com/ccao-data-science---modeling/models/ccao_condo_avm) (class 299 and 399 properties). This repository does *not* contain any modeling code, it only prepares the data used in modeling and prediction.

* `modeldata` is the set of sales used to train the machine learning model that generates assessed values. Each row represents a condominium unit sale. Properties may appear multiple times in this data set if they have multiple sales within the time range specified in the script (usually 7 years). 

* `assmntdata` is the set of all properties which need to be assessed. Each row contains one unit in a larger building and has the same property information contained in `modeldata`.

### A Note on Condominium Valuation

Unlike other residential properties, condominiums characteristics, such as square footage or number of bathrooms, are not tracked by the CCAO. As such, the vast majority of a condominium unit's value is determined by the median sale price of units within the same building. Condominium PINs are considered to be within the same building if they share the first 10 digits of a full 14-digit PIN. 

For most buildings, we can directly calculate the building median sale price and use it as a feature in our model. However, some buildings have not had a recorded sale (or arms-length sale) in a very long time. In this case, we use the previous year's FMV as a substitute for missing sales. Fortunately, there are very few buildings with no sales at all, and missing sale information is well-handled by the model.

# Inputs

Nearly all of the data used for residential modeling is stored in the CCAO's on-prem SQL database. This data is updated on a regular basis from the County mainframe using the CCAO's AS/400 system. The primary data sources include:

* **VW_RES_UNIVERSE** - A SQL view which combines tables with the goal of retrieving a set of characteristics for each PIN, year, and multicode.
* **VW_PINGEO** - Geographic location and information for each PIN, including appended data such as census tract median income, ward, tax information, etc.
* **VW_CLEAN_IDORSALES** - Sale informations and records from the Illinois Department of Revenue, including sale price and deed type.


# Outputs

Within the `output/` subfolder, the following files are created by this repository:

* `assmntdata.parquet` - Tabular dataset containing properties to be assessed, see above.
* `modeldata.parquet` - Tabular dataset containing properties used to train the assessment model, see above.
* `bldgstats.parquet` - Tabular dataset containing aggregate statistics at the building level, see note above.

These outputs are transferred as-is to the `inputs/` subfolder of the [condominium CAMA repository](https://gitlab.com/ccao-data-science---modeling/models/ccao_condo_avm), where they are used to train a machine learning model and subsequently predict assessed values.
