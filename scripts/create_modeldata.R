# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##### Query Data #####
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Query all data necessary to construct sales and assessment data samples, see
# individual query functions for more details
if (!exists("universe")) universe <- qry_universe(min_year, max_year)
if (!exists("pingeo")) pingeo <- qry_pingeo()
if (!exists("clean_sales")) clean_sales <- qry_clean_sales(min_year, max_year)




# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##### Model Data #####
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# This is the dataset used to train the condominium valuation model. It contains
# sales for all 299 and 399 class properties between min_year and max_year

# Given the universe of individual 14-digit PINs (condo units), keep information
# from the latest sale in each year between min_year and max_year, dropping all
# characteristic columns (which are empty for condos except for age)
modeldata_clean <- universe %>%
  
  # Some properties have duplicate entries for the same PIN and YEAR
  group_by(PIN, TAX_YEAR) %>%
  filter(!(n() > 1), !MULTI_IND) %>%
  ungroup() %>% 
  
  # Keep only rows in universe that have a sale for a given PIN and YEAR. Condos
  # without a sale can't be used to train the model
  inner_join(
    clean_sales,
    by = c("PIN", "TAX_YEAR")
  ) %>%
  
  # Add location data from VW_PINGEO, including lat/lon, median income, and
  # tax information such as amount paid
  mutate(pin10 = substr(PIN, 1, 10)) %>%
  left_join(
    pingeo %>%
      rename_with(function(x) paste0("geo_", tolower(x)), -c(pin10, midincome)),
    by = "pin10"
  ) %>%
  select(-pin10) %>%
  
  # Fix appended tax data by filling in missing values with tax code average,
  # or township average if tax code is missing
  group_by(TAX_CD, TAX_YEAR) %>%
  mutate(
    TAX_RATE = mean(TAX_RATE, na.rm = TRUE),
    TAX_AMT_PAID = mean(TAX_AMT_PAID, na.rm = TRUE)
  ) %>%
  group_by(TOWN_CODE, TAX_YEAR) %>%
  mutate(
    TAX_RATE = replace_na(TAX_RATE, mean(TAX_RATE, na.rm = T)),
    TAX_AMT_PAID = replace_na(TAX_AMT_PAID, mean(TAX_AMT_PAID, na.rm = T))
  ) %>%
  ungroup()

# Clean up the appended model data: order columns, correct strings, remove
# unneeded vars, etc.
modeldata_clean <- modeldata_clean %>%
  distinct(PIN, TAX_YEAR, .keep_all = TRUE) %>%
  
  # Drop all columns not relevant to condo valuation (most of them) and rename
  # the remaining columns to the standard format (meta_, char_) recorded in 
  # ccao::vars_dict
  ccao::recp_clean_keep_dict_vars(cols_to_rm = c("MLT_CD", "MULTI_CODE")) %>%
  ccao::recp_clean_rename() %>%
  select(
    -starts_with("char_"),
    -starts_with("CONDO_"),
    "char_age"
  ) %>%
  
  # Clean up string cols and add PIN10 column in order to attach
  # aggregate building information
  mutate(
    meta_class = as.character(meta_class),
    meta_town_code = stringr::str_pad(meta_town_code, 2, "left", "0"),
    meta_nbhd = stringr::str_c(
      meta_town_code, 
      stringr::str_pad(meta_nbhd, 3, "left", "0")
    )
  ) %>% 
  
  # Ensure columns have proper types and reorder/group them together based on 
  # their prefix (as recorded in ccao::vars_dict)
  ccao::recp_clean_recode() %>%
  ccao::recp_clean_relocate() %>%
  
  # Remove extremely low-value sales and any remaining garage spaces. Goal is
  # to use only arms length sales
  filter(meta_sale_price >= 1e4 & !(meta_cdu %in% c("GR")))

# Drop sales that are extremely far from the mean of each class and township
# These sales are likely not arms-length. Log transform helps normalize values
modeldata_clean <- modeldata_clean %>%
  group_by(meta_town_code) %>%
  mutate(
    lower = mean(log(meta_sale_price)) - (sd(log(meta_sale_price)) * 4),
    upper = mean(log(meta_sale_price)) + (sd(log(meta_sale_price)) * 4)
  ) %>%
  rowwise() %>%
  filter(between(log(meta_sale_price), lower, upper)) %>%
  select(-lower, -upper) %>%
  ungroup()




# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##### Feature Engineering #####
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Create new features that can be used in modeling. In the case of condos, the
# only features we can include are time features + an identifier for the
# building
modeldata_clean <- modeldata_clean %>%
  mutate(meta_bldg_pin = stringr::str_sub(meta_pin, 1, 10)) %>%
  ccao::recp_feat_time() %>%
  ccao::recp_clean_relocate()
