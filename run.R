# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##### Setup #####
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Load libraries necessary for data cleaning and reporting. Library dependencies
# for this script are recorded in renv.lock and can be loaded by running
# renv::restore()
library(arrow)
library(ccao)
library(DBI)
library(dplyr)
library(here)
library(knitr)
library(glue)
library(lubridate)
library(odbc)
library(purrr)
library(skimr)
library(tidyr)

# Load helper functions. queries.R contains parameterized SQL queries used
# to load data from the CCAO SQL database
source(here("scripts", "queries.R"))


# Set ranges of data to retrieve. These years will determine which years worth
# of data will be used when training models and when creating assessed values
# They can be set using environmental variables (to facilitate later automation)

# The maximum year of sales to include in the training data. Set by default to
# the current year minus 1
max_year <- as.numeric(ccao::model_get_env("MAX_YEAR", year(Sys.time()) - 1))

# The minimum year of sales to include in the training data. We want relatively
# recent sales but also a large enough sales sample size
min_year <- as.numeric(ccao::model_get_env("MIN_YEAR", max_year - 7))

# The year for which properties should be assessed. Goal of the model is to
# determine the sale price of all properties were they to sell on Jan 1st of
# assmnt_year
assmnt_year <- as.numeric(ccao::model_get_env("ASSESSMENT_YEAR", max_year + 1))

# The year to draw characteristics data from for assessment. Typically
# should be assessment year minus one (if property is being assessed for
# Jan 1, 2021, we want characteristics from 2020)
chars_year <- as.numeric(ccao::model_get_env("CHARS_YEAR", assmnt_year - 1))

# The minimum year of sales to include when calculating the building-level mean
# specific to the condo model. This will be used to find sales within the
# building within the last X years, then average them to create a new feature
# for the condo model
bldg_min_year <- as.numeric(ccao::model_get_env("BLDG_MIN_YEAR", chars_year - 5))


# Connect to main CCAO DB. Here connection credentials are loaded from env var
# Only works while connected to CCAO's VPN or in office
CCAODATA <- dbConnect(
  odbc(),
  .connection_string = Sys.getenv("DB_CONFIG_CCAODATA")
)




# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##### Gather Data #####
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Create modeldata, which contains the sales data used to train the assessment
# model. In machine learning terms, this is the training/test set
source(here("scripts", "create_modeldata.R"))

# Create strata of building-level, previous-5-year sale prices. These strata are
# used as categorical variables in the model
source(here("scripts", "create_bldgstrata.R"))

# Attach each strata level then save modeldata to file
# This file is then loaded by the model.R script contained here:
# https://gitlab.com/ccao-data-science---modeling/models/ccao_condo_avm
modeldata_clean <- modeldata_clean %>%
  left_join(bldg_avg_w_target) %>%
  bind_cols(bldg_strata) %>% 
  mutate(
    meta_strata_10 = ccao:::val_assign_ntile(
      meta_sale_avg_w_target,
      meta_strata_groups_10
    ),
    meta_strata_300 = ccao:::val_assign_ntile(
      meta_sale_avg_w_target,
      meta_strata_groups_300
    )
  ) %>%
  select(
    -meta_sale_avg_w_target,
    -meta_strata_groups_10, 
    -meta_strata_groups_300
  ) %>%
  arrow::write_parquet(here("output", "modeldata.parquet"))

# Create assmntdata, which is the actual data used when predicting/assessing
# all property values. In machine learning terms, this is the prediction set
source(here("scripts", "create_assmntdata.R"))

# Save cleaned assmntdata to parquet file
# This file is then loaded by the valuation.R script contained here:
# https://gitlab.com/ccao-data-science---modeling/models/ccao_condo_avm
assmntdata_clean <- assmntdata_clean %>%
  left_join(bldg_avg_w_target) %>%
  bind_cols(bldg_strata) %>% 
  mutate(
    meta_strata_10 = ccao:::val_assign_ntile(
      meta_sale_avg_w_target,
      meta_strata_groups_10
    ),
    meta_strata_300 = ccao:::val_assign_ntile(
      meta_sale_avg_w_target,
      meta_strata_groups_300
    )
  ) %>%
  select(
    -meta_sale_avg_w_target,
    -meta_strata_groups_10, 
    -meta_strata_groups_300
  ) %>%
  arrow::write_parquet(here("output", "assmntdata.parquet"))

# Disconnect from database
dbDisconnect(CCAODATA)
